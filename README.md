# LIMDb Project
This project implements a mini version of IMDb.


## Testing
I use **selenium** to simulate user behaviors. (see _accounts/tests.py_)
![img_7.png](img_7.png)

## Features 
### Sign up

![img.png](img.png)

### Log in

![img_1.png](img_1.png)

### Log out

![img_2.png](img_2.png)

## View list of Movies

![img_3.png](img_3.png)

## Create new movie

![img_4.png](img_4.png)

## Edit a movie

![img_5.png](img_5.png)


## View movie information and leave a comment 

![img_6.png](img_6.png)

## And the same with Actors and Rewards
