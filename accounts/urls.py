from django.urls import re_path

from accounts import views as accounts_views
from django.contrib.auth import views as auth_views


urlpatterns = [
    re_path(r'^signup/$', accounts_views.sign_up, name='sign-up'),
    re_path(r'^login/$', auth_views.LoginView.as_view(template_name='accounts/login.html'), name='log-in'),
    re_path(r'^logout/$', accounts_views.log_out, name='log-out'),
]