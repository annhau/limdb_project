from django.contrib.auth.models import User
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

PASSWORD = 'LgDKVzFZdnqwC5y'


def log_in(browser):
    browser.find_element(By.ID, 'id_username').send_keys('alice')
    browser.find_element(By.ID, 'id_password').send_keys(PASSWORD + Keys.ENTER)


def create_user(username):
    # Create account for alice
    user = User.objects.create(username=username, is_active=True)
    user.set_password(PASSWORD)
    user.save()
    return user


class AuthenticationTest(LiveServerTestCase):

    def setUp(self):
        create_user('alice')
        # Alice visits our web site
        self.browser = webdriver.Chrome()
        self.browser.get(self.live_server_url)

    def tearDown(self):
        # Alice closes her browser
        self.browser.close()

    def test_log_in(self):
        browser = self.browser

        # She was redirected to login page
        self.assertTrue(browser.current_url,
                        self.live_server_url + 'login/?next=/')

        # She enters her username and password
        browser.find_element(By.ID, 'id_username').send_keys('zalice')
        browser.find_element(By.ID, 'id_password').send_keys(
            PASSWORD + Keys.ENTER)

        # But she enters wrong password, so she saw a error message
        message = "Please enter a correct username and password. " \
                  "Note that both fields may be case-sensitive."
        error = browser.find_element(By.CLASS_NAME, 'alert') \
            .find_element(By.TAG_NAME, 'p').text
        self.assertEqual(message, error)

        # Then she tried again
        browser.find_element(By.ID, 'id_username').send_keys(Keys.DELETE)
        browser.find_element(By.ID, 'id_password').send_keys(
            PASSWORD + Keys.ENTER)

        # Now she's in the main page
        self.assertEqual(browser.current_url, self.live_server_url + '/')

    def test_log_out(self):
        browser = self.browser

        # Alice logs in using her password
        log_in(browser)

        # She saw a log-out link
        log_out = browser.find_element(By.XPATH, '/html/body/nav/div/div[2]/ul/li/a')
        self.assertTrue(log_out.get_attribute('href'),
                        self.live_server_url + '/logout/')

        # She tried it, and get redirect to log-out page
        log_out.click()
        self.assertEqual(self.live_server_url + '/logout/', browser.current_url)
        self.assertIn('Logged out !',
                      browser.find_element(By.TAG_NAME, 'body').text)

    def test_sign_up(self):
        browser = self.browser

        # John comes to our website, he clicks the sign-up link
        browser.find_element(By.XPATH, '/html/body/nav/div/div[2]/ul/li[2]/a').click()

        # He was redirected to sign up page
        self.assertEqual(self.live_server_url + '/signup/', browser.current_url)

        # He tries to create a user with username 'alice'
        browser.find_element(By.ID, 'id_username').send_keys('alice')
        browser.find_element(By.ID, 'id_password1').send_keys(PASSWORD)
        browser.find_element(By.ID, 'id_password2').send_keys(
            PASSWORD + Keys.ENTER)

        # He got an error
        error = browser.find_element(By.CLASS_NAME, 'invalid-feedback')
        self.assertEqual(error.text.strip(),
                         'A user with that username already exists.')

        # Now he tries username 'john'
        browser.find_element(By.ID, 'id_username').send_keys(Keys.DELETE * 10)
        browser.find_element(By.ID, 'id_username').send_keys('john')
        browser.find_element(By.ID, 'id_password1').send_keys(PASSWORD)
        browser.find_element(By.ID, 'id_password2').send_keys(
            PASSWORD + Keys.ENTER)

        # Succeed, he gets redirect to the main page
        self.assertEqual(browser.current_url, self.live_server_url + '/')
