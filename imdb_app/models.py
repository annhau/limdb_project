from django.contrib.auth.models import User
from django.db import models


GENDER = (
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other')
)

AWARD_KIND = (
    ('actor', 'Actor'),
    ('movie', 'Movie')
)


class Category(models.Model):
    code = models.CharField(max_length=20)
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Movie(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=4000)
    release_date = models.DateField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE,)
    actors = models.ManyToManyField('Actor', blank=True)
    logo = models.FileField(upload_to='pictures', null=True, blank=True)

    def static_url(self):
        return 'static/' + self.logo.name.split('/')[-1]

    def __str__(self):
        return self.title


class Actor(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    birth_date = models.DateField()
    sex = models.CharField(choices=GENDER, max_length=10)
    nationality = models.CharField(max_length=255)
    alive = models.BooleanField(default=True)

    @property
    def name(self):
        return self.first_name + ' ' + self.last_name

    def __str__(self):
        return self.name


class Award(models.Model):
    name = models.CharField(max_length=255)
    kind = models.CharField(choices=AWARD_KIND, max_length=10)
    movies = models.ManyToManyField(Movie, blank=True)
    actors = models.ManyToManyField(Actor, blank=True)


class Comment(models.Model):
    text = models.TextField(max_length=1000)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING,)
    movie = models.ForeignKey(Movie, on_delete=models.DO_NOTHING,)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
