from django.urls import reverse
from mixer.backend.django import mixer
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from accounts.tests import create_user, log_in
from .models import Movie, Category, Actor, Award, Comment
from datetime import datetime


class TestMixin(StaticLiveServerTestCase):
    def setUp(self):
        self.user = create_user('alice')
        self.other_user = create_user('newton')
        action = Category.objects.create(code='action', name='Action')
        title_list = ['Avengers', 'Hell Boy', 'Warcraft']
        self.movies = []
        for title in title_list:
            movie = Movie.objects.create(title=title,
                                         description='description for ' + title,
                                         release_date=datetime.now(),
                                         category=action)
            self.movies.append(movie)
        self.actors = mixer.cycle(5).blend(Actor)

        self.actor_award = mixer.blend(Award, kind='actor', actors=self.actors)
        self.movie_award = mixer.blend(Award, kind='movie', movies=self.movies)

        self.alice_comment = Comment.objects.create(user=self.user,
                                                    movie=self.movies[0],
                                                    text='Hello')
        self.newton_comment = Comment.objects.create(user=self.other_user,
                                                     movie=self.movies[0],
                                                     text='I am Newton.')

        # Alice visits our movie site
        self.browser = webdriver.Chrome()

    def tearDown(self):
        # Alice closes her browser
        self.browser.close()

    def check_require_log_in(self, url, redirect=False):
        # Alice want to go `url`
        browser = self.browser
        browser.get(self.live_server_url + url)

        # She is required to log in
        self.assertTrue(browser.current_url,
                        self.live_server_url + '/login/?next=' + url)

        # She logged in.
        log_in(browser)
        if not redirect:
            self.assertEqual(browser.current_url,
                             self.live_server_url + url)


class TestMovie(TestMixin):
    def test_list_view(self):
        # Alice go to the movie page
        browser = self.browser
        self.check_require_log_in('/movie/')
        self.assertEqual('Movies', browser.title)

        # She saw a list of 3 movies.
        items = browser.find_elements_by_class_name('list-group-item')
        self.assertEqual(len(items), 3)

        # Movie Avengers is on top, it contains title and description
        movie_avenger = items[0]
        self.assertEqual(movie_avenger.text,
                         'Avengers\ndescription for Avengers')

        # The logo is default
        logo = movie_avenger.find_element_by_tag_name('img').get_attribute(
            'src')
        self.assertEqual(self.live_server_url + '/static/nopicture.gif', logo)

        # There're hidden buttons
        body = movie_avenger.find_element_by_class_name('media-body')
        style = body.find_element_by_id('toHideDiv').get_attribute('style')
        self.assertEqual(style, 'display: none;')
        # She clicks on the body, and saw hidden buttons
        body.click()
        style = body.find_element_by_id('toHideDiv').get_attribute('style')
        self.assertEqual(style, 'display: block;')

    def test_detail(self):
        avenger = self.movies[0]
        url = '/movie/%s/' % avenger.pk
        browser = self.browser

        # Alice wants to view avenger detail
        self.check_require_log_in(url)
        self.assertEqual('Movies - ' + avenger.title, browser.title)

        # Detail information will be shown
        content = browser.find_element_by_tag_name('body').text
        self.assertIn(avenger.title, content)
        self.assertIn(avenger.release_date.strftime('%B %d, %Y'), content)
        self.assertIn(avenger.description, content)
        self.assertIn(avenger.category.name, content)

    def test_create(self):
        # Alice wants to create movie
        browser = self.browser
        url = '/movie/create/'
        self.check_require_log_in(url)

        self.assertEqual('Movies - create', browser.title)

        # She leaves the form blank and submit
        browser.find_element_by_xpath('/html/body/div/form/button').click()

        # It goes back to the form with errors
        self.assertEqual(self.live_server_url + url, browser.current_url)
        self.assertIn('This field is required',
                      browser.find_element_by_tag_name('body').text)

        # This time she filled all the required fields
        browser.find_element_by_xpath('//*[@id="id_title"]').send_keys(
            'A danger place')
        browser.find_element_by_xpath('//*[@id="id_description"]').send_keys(
            'An awesome movie')
        browser.find_element_by_xpath('//*[@id="id_release_date"]').send_keys(
            '01012011')
        browser.find_element_by_xpath('//*[@id="id_category"]').send_keys(
            Keys.DOWN)
        browser.find_element_by_xpath('/html/body/div/form/button').click()

        # Succeed. She was redirected to the movie page, and see the new movie
        new_movie = Movie.objects.last()
        url = self.live_server_url + '/movie/{}/'.format(new_movie.pk)
        self.assertEqual(url, browser.current_url)
        self.assertIn('A danger place',
                      browser.find_element_by_tag_name('body').text)

    def test_delete(self):
        avenger = self.movies[0]
        browser = self.browser
        url = '/movie/{}/delete/'.format(avenger.pk)
        self.check_require_log_in(url, redirect=True)

        # After logged in, delete succeed and go back to the movie page
        self.assertEqual(self.live_server_url + '/movie/', browser.current_url)

        # Avenger not in the list anymore
        self.assertNotIn(avenger.title,
                         browser.find_element_by_tag_name('body').text)
        self.assertIn(self.movies[1].title,
                      browser.find_element_by_tag_name('body').text)
        self.assertIn(self.movies[2].title,
                      browser.find_element_by_tag_name('body').text)

    def test_edit(self):
        browser = self.browser
        movie = self.movies[0]
        url = '/movie/{}/edit/'.format(movie.pk)
        self.check_require_log_in(url)
        browser.find_element_by_xpath('/html/body/div/form/button').click()
        self.assertEqual(self.live_server_url + '/movie/{}/'.format(movie.pk),
                         browser.current_url)


class TestActor(TestMixin):
    def test_list_view(self):
        browser = self.browser
        url = '/actor/'
        self.check_require_log_in(url)

        # Alice sees a list of actors
        self.assertEqual('Actors', browser.title)
        for actor in self.actors:
            self.assertIn(actor.name,
                          browser.find_element_by_tag_name('body').text)

        # She clicks on the first movie and see hidden buttons
        hidden = browser.find_element_by_xpath('//*[@id="toHideDiv"]')
        self.assertEqual(hidden.get_attribute('style'), 'display: none;')
        first_movie = browser.find_element_by_xpath(
            '/html/body/div/div[1]/ul/li[1]')
        first_movie.click()
        self.assertEqual(hidden.get_attribute('style'), 'display: block;')

    def test_detail(self):
        browser = self.browser
        actor = self.actors[0]
        url = '/actor/{}/'.format(actor.pk)
        self.check_require_log_in(url)
        self.assertEqual('Actors - ' + actor.name, browser.title)

        # All the information should in
        content = browser.find_element_by_tag_name('body').text
        self.assertIn(actor.name, content)
        self.assertIn(actor.sex.capitalize(), content)
        self.assertIn(actor.nationality, content)
        for movie in actor.movie_set.all():
            self.assertIn(movie.title, content)

    def test_create(self):
        browser = self.browser
        url = '/actor/create/'
        self.check_require_log_in(url)
        self.assertEqual('Actors - create', browser.title)

        # She leaves the form blank.
        browser.find_element_by_xpath('/html/body/div/form/button').click()
        self.assertEqual(self.live_server_url + '/actor/create/',
                         browser.current_url)
        self.assertIn('This field is required',
                      browser.find_element_by_tag_name('body').text)

        # She filled the form
        browser.find_element_by_xpath('//*[@id="id_first_name"]').send_keys(
            'Alice')
        browser.find_element_by_xpath('//*[@id="id_last_name"]').send_keys(
            'Johnson')
        browser.find_element_by_xpath('//*[@id="id_birth_date"]').send_keys(
            '010102000')
        browser.find_element_by_xpath('//*[@id="id_sex"]').send_keys(
            Keys.DOWN + Keys.DOWN)
        browser.find_element_by_xpath('//*[@id="id_nationality"]').send_keys(
            'USA')
        browser.find_element_by_xpath('/html/body/div/form/button').click()

        # Succeed. Be redirected to actor page
        new_actor = Actor.objects.get(first_name='Alice', last_name='Johnson')
        self.assertEqual(self.live_server_url
                         + '/actor/{}/'.format(new_actor.pk),
                         browser.current_url)
        # See new actor
        self.assertIn('Alice Johnson',
                      browser.find_element_by_tag_name('body').text)

    def test_delete(self):
        browser = self.browser
        actor = self.actors[0]
        url = '/actor/{}/delete'.format(actor.pk)
        self.check_require_log_in(url, redirect=True)
        self.assertEqual(self.live_server_url + '/actor/', browser.current_url)
        self.assertNotIn(actor.name,
                         browser.find_element_by_tag_name('body').text)

    def test_edit(self):
        browser = self.browser
        actor = self.actors[0]
        url = '/actor/{}/edit/'.format(actor.pk)
        self.check_require_log_in(url)
        browser.find_element_by_xpath('/html/body/div/form/button').click()
        self.assertEqual(self.live_server_url + '/actor/{}/'.format(actor.pk),
                         browser.current_url)


class TestAward(TestMixin):
    def test_list_view(self):
        browser = self.browser
        self.check_require_log_in('/award/')

        self.assertEqual('Awards', browser.title)
        content = browser.find_element_by_tag_name('body').text
        self.assertIn(self.movie_award.name, content)
        self.assertIn(self.actor_award.name, content)

    def test_create(self):
        browser = self.browser
        url = '/award/create/'
        self.check_require_log_in(url)
        self.assertEqual('Awards - create', browser.title)

        # She leaves the form blank.
        browser.find_element_by_xpath('/html/body/div/form/button').click()
        self.assertEqual(self.live_server_url + url, browser.current_url)
        self.assertIn('This field is required',
                      browser.find_element_by_tag_name('body').text)

        # She filled the form
        browser.find_element_by_xpath('//*[@id="id_name"]').send_keys(
            'Big Award')

        # Kind is none, so actors group and movies group haven't show yet.
        actors_group = browser.find_element_by_xpath('//*[@id="grpActors"]')
        movies_group = browser.find_element_by_xpath('//*[@id="grpMovies"]')
        self.assertEqual(actors_group.get_attribute('style'), 'display: none;')
        self.assertEqual(movies_group.get_attribute('style'), 'display: none;')

        # Choose actor as kind
        browser.find_element_by_xpath('//*[@id="id_kind"]').send_keys(Keys.DOWN)
        # Actors group shows, but Movies group doens't.
        self.assertNotEqual(actors_group.get_attribute('style'),
                            'display: none;')
        self.assertEqual(movies_group.get_attribute('style'), 'display: none;')

        # Choose movie as kind
        browser.find_element_by_xpath('//*[@id="id_kind"]').send_keys(Keys.DOWN)
        self.assertEqual(actors_group.get_attribute('style'),
                         'display: none;')
        self.assertNotEqual(movies_group.get_attribute('style'),
                            'display: none;')

        # Submit
        browser.find_element_by_xpath('/html/body/div/form/button').click()

        # Succeed. Be redirected to actor page
        new_award = Award.objects.last()
        self.assertEqual(self.live_server_url
                         + '/award/{}/'.format(new_award.pk),
                         browser.current_url)
        # See new award
        self.assertIn('Big Award',
                      browser.find_element_by_tag_name('body').text)

    def test_detail(self):
        browser = self.browser
        award = self.movie_award
        url = '/award/{}/'.format(award.pk)
        self.check_require_log_in(url)
        self.assertEqual('Awards - ' + award.name, browser.title)

        # All the information should in
        content = browser.find_element_by_tag_name('body').text
        self.assertIn(award.name, content)
        self.assertIn(award.kind.capitalize(), content)
        for movie in award.movies.all():
            self.assertIn(movie.title, content)
        for actor in award.actors.all():
            self.assertIn(actor.name, content)

    def test_delete(self):
        browser = self.browser
        award = self.actor_award
        url = '/award/{}/delete'.format(award.pk)
        self.check_require_log_in(url, redirect=True)
        self.assertEqual(self.live_server_url + '/award/', browser.current_url)
        self.assertNotIn(award.name,
                         browser.find_element_by_tag_name('body').text)
        self.assertIn(self.movie_award.name,
                      browser.find_element_by_tag_name('body').text)

    def test_edit(self):
        browser = self.browser
        award = self.actor_award
        url = reverse('award-edit', args=(award.pk,))
        self.check_require_log_in(url)

        # Select a actor
        browser.find_element_by_xpath('//*[@id="id_actors"]') \
            .send_keys(Keys.DOWN + Keys.DOWN)
        # Submit
        browser.find_element_by_xpath('/html/body/div/form/button').click()
        self.assertEqual(self.live_server_url + '/award/{}/'.format(award.pk),
                         browser.current_url)


class TestComment(TestMixin):
    def test_post_comment(self):
        browser = self.browser
        movie = self.movies[0]
        url = reverse('movie-detail', args=(movie.pk,))

        # Alice come to a movie and post comment
        self.check_require_log_in(url)
        message = 'Hello everybody'
        browser.find_element_by_xpath('//*[@id="comment"]').send_keys(message)
        browser.find_element_by_xpath('//*[@id="comment_form"]/button').click()

        content = browser.find_element_by_tag_name('body').text
        self.assertIn(self.user.username, content)
        self.assertIn(message, content)

    def test_edit_comment(self):
        browser = self.browser
        url = reverse('comment-edit', args=(self.newton_comment.pk,))
        self.check_require_log_in(url)

        # Alice cannot edit newton's comment
        self.assertIn('HTTP ERROR 403',
                      browser.find_element_by_tag_name('body').text)

        # Alice can edit her comment
        browser.get(self.live_server_url + reverse('comment-edit', args=(
            self.alice_comment.pk,)))
        old_comment = self.alice_comment.text
        new_comment = 'Good bye.'

        browser.find_element_by_xpath('//*[@id="id_text"]').send_keys(
            Keys.BACK_SPACE * 30)
        browser.find_element_by_xpath('//*[@id="id_text"]').send_keys(
            new_comment)
        browser.find_element_by_xpath('/html/body/div/form/button').click()

        content = browser.find_element_by_tag_name('body').text
        self.assertNotIn(old_comment, content)
        self.assertIn(new_comment, content)

    def test_delete_comment(self):
        browser = self.browser
        url = reverse('comment-delete', args=(self.newton_comment.pk,))
        self.check_require_log_in(url)

        # Alice cannot delete newton's comment
        self.assertIn('HTTP ERROR 403',
                      browser.find_element_by_tag_name('body').text)

        # Alice can edit her comment
        browser.get(self.live_server_url + reverse('comment-delete', args=(
            self.alice_comment.pk,)))

        content = browser.find_element_by_tag_name('body').text
        self.assertNotIn(self.alice_comment.text, content)
        self.assertIn(self.newton_comment.text, content)
