from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, \
    DeleteView

from imdb_app.models import Movie, Comment


class MovieListView(LoginRequiredMixin, ListView):
    model = Movie
    context_object_name = 'movies'
    template_name = 'imdb/movies.html'
    paginate_by = 5

    def get_queryset(self):
        return Movie.objects.all().order_by('title')


class MovieDetailView(LoginRequiredMixin, DetailView):
    model = Movie
    context_object_name = 'movie'
    template_name = 'imdb/movie_detail.html'

    def get_context_data(self, **kwargs):
        context = super(MovieDetailView, self).get_context_data(**kwargs)
        comments = Comment.objects.filter(movie_id=self.kwargs['pk'])
        context['comments'] = comments
        return context

    def dispatch(self, request, *args, **kwargs):
        if request.method == 'POST':
            text = request.POST.get('text', '')
            if text.strip():
                Comment.objects.create(text=text, user=request.user,
                                       movie_id=self.kwargs['pk'])
            return HttpResponseRedirect(request.path)

        return super(MovieDetailView, self).dispatch(request, *args, **kwargs)


class MovieCreateView(LoginRequiredMixin, CreateView):
    model = Movie
    fields = '__all__'

    template_name = 'imdb/movie_create.html'

    def get_success_url(self):
        return reverse_lazy('movie-detail', kwargs={'pk': self.object.pk})


class MovieEditView(LoginRequiredMixin, UpdateView):
    model = Movie
    fields = '__all__'
    context_object_name = 'movie'
    template_name = 'imdb/movie_edit.html'

    get_success_url = MovieCreateView.get_success_url


class MovieDeleteView(LoginRequiredMixin, DeleteView):
    model = Movie

    def get_success_url(self):
        return reverse('movie')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
