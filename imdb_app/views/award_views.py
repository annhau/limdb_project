from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, \
    DeleteView

from imdb_app.models import Award


class AwardListView(LoginRequiredMixin, ListView):
    model = Award
    context_object_name = 'awards'
    template_name = 'imdb/awards.html'
    paginate_by = 10

    def get_queryset(self):
        awards = list(Award.objects.all().order_by('name'))
        actor_awards = [a for a in awards if a.kind == 'actor']
        movie_awards = [a for a in awards if a.kind != 'actor']
        awards = []
        while actor_awards or movie_awards:
            if actor_awards:
                awards.append(actor_awards.pop(0))
            if movie_awards:
                awards.append(movie_awards.pop(0))
        return awards


class AwardDetailView(LoginRequiredMixin, DetailView):
    model = Award
    context_object_name = 'award'
    template_name = 'imdb/award_detail.html'


class AwardCreateView(LoginRequiredMixin, CreateView):
    model = Award
    fields = '__all__'
    template_name = 'imdb/award_create.html'

    def form_valid(self, form):
        if form.cleaned_data['kind'] == 'actor':
            form.cleaned_data['movies'] = Award.objects.none()
        else:
            form.cleaned_data['actors'] = Award.objects.none()
        form.save()
        url = reverse_lazy('award-detail', kwargs={'pk': form.instance.pk})
        return HttpResponseRedirect(url)


class AwardEditView(LoginRequiredMixin, UpdateView):
    model = Award
    fields = '__all__'
    context_object_name = 'award'
    template_name = 'imdb/award_edit.html'

    form_valid = AwardCreateView.form_valid


class AwardDeleteView(LoginRequiredMixin, DeleteView):
    model = Award

    def get_success_url(self):
        return reverse('award')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
