from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, \
    DeleteView

from imdb_app.models import Actor


class ActorListView(LoginRequiredMixin, ListView):
    model = Actor
    context_object_name = 'actors'
    template_name = 'imdb/actors.html'
    paginate_by = 10

    def get_queryset(self):
        actors = Actor.objects.all().order_by('first_name')
        return actors


class ActorDetailView(LoginRequiredMixin, DetailView):
    model = Actor
    context_object_name = 'actor'
    template_name = 'imdb/actor_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ActorDetailView, self).get_context_data(**kwargs)
        context['movies'] = self.get_object().movie_set.all()
        return context


class ActorCreateView(LoginRequiredMixin, CreateView):
    model = Actor
    fields = '__all__'
    template_name = 'imdb/actor_create.html'

    def get_success_url(self):
        return reverse_lazy('actor-detail', kwargs={'pk': self.object.pk})


class ActorEditView(LoginRequiredMixin, UpdateView):
    model = Actor
    fields = '__all__'
    context_object_name = 'actor'
    template_name = 'imdb/actor_edit.html'

    get_success_url = ActorCreateView.get_success_url


class ActorDeleteView(LoginRequiredMixin, DeleteView):
    model = Actor

    def get_success_url(self):
        return reverse('actor')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
