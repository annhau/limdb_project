from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseForbidden
from django.views.generic import UpdateView, DeleteView

from imdb_app.models import Comment


class CommentUpdateView(LoginRequiredMixin, UpdateView):
    model = Comment
    fields = 'text',
    context_object_name = 'comment'
    template_name = 'imdb/comment_edit.html'

    def get_success_url(self):
        return '/movie/' + str(self.object.movie_id)

    def get(self, request, *args, **kwargs):
        if self.get_object().user == self.request.user:
            return super(CommentUpdateView, self).get(self, request, *args, **kwargs)
        return HttpResponseForbidden()


class CommentDeleteView(LoginRequiredMixin, DeleteView):
    model = Comment

    def get_success_url(self):
        return '/movie/' + str(self.object.movie_id)

    def get(self, request, *args, **kwargs):
        if self.get_object().user == self.request.user:
            return self.post(request, *args, **kwargs)
        return HttpResponseForbidden()