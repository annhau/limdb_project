from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required(login_url='log-in')
def home(request):
    return render(request, 'imdb/home.html')