from .actor_views import ActorDeleteView, ActorListView, ActorCreateView, \
    ActorDetailView, ActorEditView
from .award_views import AwardCreateView, AwardDeleteView, AwardDetailView, \
    AwardEditView, AwardListView
from .movie_views import MovieEditView, MovieCreateView, MovieDetailView, \
    MovieListView, MovieDeleteView
from .comment_views import CommentDeleteView, CommentUpdateView
from .base_views import home
