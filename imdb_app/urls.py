from django.conf.urls.static import static
from django.urls import path
from django_imdb import settings
import imdb_app.views as views


urlpatterns = [
    path('', views.home, name='home'),

    # Movie Routes
    path('movie/', views.MovieListView.as_view(), name='movie'),
    path('movie/create/', views.MovieCreateView.as_view(), name='movie-create'),
    path('movie/<int:pk>/', views.MovieDetailView.as_view(), name='movie-detail'),
    path('movie/<int:pk>/edit/', views.MovieEditView.as_view(), name='movie-edit'),
    path('movie/<int:pk>/delete/', views.MovieDeleteView.as_view(), name='movie-delete'),

    # Comment routes
    path('comment/<int:pk>/edit', views.CommentUpdateView.as_view(), name='comment-edit'),
    path('comment/<int:pk>/delete', views.CommentDeleteView.as_view(), name='comment-delete'),

    # Actor Routes
    path('actor/', views.ActorListView.as_view(), name='actor'),
    path('actor/create/', views.ActorCreateView.as_view(), name='actor-create'),
    path('actor/<int:pk>/', views.ActorDetailView.as_view(), name='actor-detail'),
    path('actor/<int:pk>/edit/', views.ActorEditView.as_view(), name='actor-edit'),
    path('actor/<int:pk>/delete/', views.ActorDeleteView.as_view(), name='actor-delete'),

    # Award Routes
    path('award/', views.AwardListView.as_view(), name='award'),
    path('award/create/', views.AwardCreateView.as_view(), name='award-create'),
    path('award/<int:pk>/', views.AwardDetailView.as_view(), name='award-detail'),
    path('award/<int:pk>/edit/', views.AwardEditView.as_view(), name='award-edit'),
    path('award/<int:pk>/delete/', views.AwardDeleteView.as_view(), name='award-delete')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
